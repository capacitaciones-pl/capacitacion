package pe.pangolabs.app_gastos.demo.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("api/v1/demo")
public class DemoController {

    @GetMapping()
    public ResponseEntity<Double> getDemo() {
        //return ResponseEntity.ok("Hola Mundo!!!!");
        //return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Hola Mundo Error!!!");
        //double a = 5 / 0.0 ;
        //int a = 5 / 0;
        try {
            int a = 5 / 0;
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR , ex.getMessage(), ex);
        }
        return ResponseEntity.ok(new Double(0.0));
    }
}
